function cacheFunction(callback) {
    let object = {};
    return function (num) {
        if (object[num]) {
            return object[num];
        }
        else {
            let result = callback(num);
            object[num] = result;
            return result;
        };
    };
};

function factorial(number) {
    if (number === 1 || number === 0) {
        return 1;
    }
    return number * factorial(number - 1);
};

module.exports.cacheFunction = cacheFunction;
module.exports.factorial = factorial;