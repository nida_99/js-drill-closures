function counterFactory() {
    let counter = 1;
    return obj = {
        increment: function () {
            counter++;
            return counter;
        },
        decrement: function () {
            counter--;
            return counter;
        }

    };
};

module.exports.counterFactory = counterFactory;







