function limitFunctionCallCount(callback, n) {
    let start = 0;

    return function () {
        if (start < n) {
            start++;
            return callback(start);
        }
        else {
            return null;
        };
    };
};
function printElement(num) {
    return num;

};

module.exports.limitFunctionCallCount = limitFunctionCallCount;
module.exports.printElement = printElement;



