const counterFactory = require('../counterFactory.js').counterFactory;
const value = counterFactory();

let expectedOutput1 = 2;
let actualOutput1 = value.increment();

let expectedOutput2 = 1;
let actualOutput2 = value.decrement();

let correctAnswer = true;
if (actualOutput1 !== expectedOutput1 || actualOutput2 !== expectedOutput2) {
    correctAnswer = false;
}

if (correctAnswer) {
    console.log("Function is working properly");
}
else {
    console.log("Check the function again");
};