const cacheFunction = require('../cacheFunction.js').cacheFunction;
const factorial = require('../cacheFunction.js').factorial;
let data = 5;
let expectedOutput = 120;

let value = cacheFunction(factorial);
let actualOutput = value(data);

if (actualOutput === expectedOutput) {
    console.log("Function is working properly");
} else {
    console.log("Check the function again");
};