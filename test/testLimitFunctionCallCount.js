const limitFunctionCallCount = require('../limitFunctionCallCount.js').limitFunctionCallCount;
const printElement = require('../limitFunctionCallCount.js').printElement;
let num = 2;
const value = limitFunctionCallCount(printElement, num);

const expectedOutput1 = 1;
const expectedOutput2 = 2;
const expectedOutput3 = null;

const actualOutput1 = value();
const actualOutput2 = value();
const actualOutput3 = value();

let correctAnswer = true;

if (actualOutput1 !== expectedOutput1 || actualOutput2 !== expectedOutput2 || actualOutput3 !== expectedOutput3) {
    correctAnswer = false;
}
if (correctAnswer) {
    console.log("function is working properly");
} else {
    console.log("Check the function again");
};





